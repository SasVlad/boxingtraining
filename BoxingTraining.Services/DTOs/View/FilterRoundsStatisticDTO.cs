﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.View
{
    public class FilterRoundsStatisticDTO
    {
        public Guid SportUserId { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
