﻿using BoxingTraining.BLL.DTOs.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.View
{
    public class CreateSportsmanDTO
    {
        public CreateSportsmanDTO()
        {
            SportsmanInformation = new SportUserDTO();
        }
        public SportUserDTO SportsmanInformation { get; set; }
        public Guid TrainerId { get; set; }
    }
}
