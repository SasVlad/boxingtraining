﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.View
{
    public class PersonRoundStatisticDTO
    {
        public SportUser SportUser { get; set; }
        public List<RoundStatisticDTO> PersonRaundsStatistic { get; set; }
    }
}
