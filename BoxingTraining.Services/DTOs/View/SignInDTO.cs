﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.View
{
    public class SignInDTO:BaseEntity
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
