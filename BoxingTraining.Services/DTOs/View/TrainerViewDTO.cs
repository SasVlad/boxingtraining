﻿using BoxingTraining.BLL.DTOs.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.View
{
    public class TrainerViewDTO
    {
        public TrainerViewDTO()
        {
            GeneralInformation = new SportUserDTO();
            AuthenticateInformation = new SignInDTO();
        }
        public SportUserDTO GeneralInformation { get; set; }
        public SignInDTO AuthenticateInformation { get; set; }
    }
}
