﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.Entity
{
    public class TrainerSportsmanDTO : BaseEntity
    {
        public Guid TrainerId { get; set; }
        public Guid SportsmanId { get; set; }       
    }
}
