﻿
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.Entity
{
    public class SportUserDTO:BaseEntity
    {
        public string FirstAndLastName { get; set; }
    }
}
