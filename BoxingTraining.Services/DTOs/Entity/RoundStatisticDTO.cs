﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.DTOs.Entity
{
    public class RoundStatisticDTO : BaseEntity
    {
        public Guid SportUserId { get; set; }
        public int CountRounds { get; set; }
        public string DateRecord { get; set; }
    }
}
