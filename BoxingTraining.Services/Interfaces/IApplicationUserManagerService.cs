﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.DTOs.View;
using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Interfaces
{
    public interface IApplicationUserManagerService : IGenericService<ApplicationUser, SignInDTO>
    {
        Task<ApplicationUserDTO> FindAsync(SignInDTO entity);
    }
}
