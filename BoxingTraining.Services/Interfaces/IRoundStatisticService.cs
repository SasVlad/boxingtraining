﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.DTOs.View;
using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Interfaces
{
    public interface IRoundStatisticService : IGenericService<RoundStatistic, RoundStatisticDTO>
    {
        Task<List<RoundStatisticDTO>> GetFilterRoundsStatisticAsync(FilterRoundsStatisticDTO filterRoundsStatisticDTO);
        Task<bool> AddOrUpdateRecordAsync(RoundStatisticDTO entityDto);
    }
}
