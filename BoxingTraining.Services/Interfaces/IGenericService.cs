﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Interfaces
{
    public interface IGenericService<TEntity,TEntityDTO>
        where TEntity: class
        where TEntityDTO: class
    {
        Task<bool> AddAsync(TEntityDTO entityDto);
        Task<bool> DeleteAsync(Guid Id);
        Task<bool> DeleteWithConditionAsync(string where = null);
        Task<bool> UpdateAsync(TEntityDTO entity);
        Task<List<TEntityDTO>> GetAllAsync(string where = null);
        Task<TEntityDTO> GetAsync(Guid Id);
    }
}
