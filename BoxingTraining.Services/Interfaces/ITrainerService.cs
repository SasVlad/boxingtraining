﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Interfaces
{
    public interface ITrainerService : IGenericService<SportUser, SportUserDTO>
    {
        Task<List<SportUserDTO>> GetTrainerSportsmans(Guid trainerId);
    }
}
