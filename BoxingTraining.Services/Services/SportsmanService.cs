﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Interfaces;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BoxingTraining.BLL.DTOs.View;
using BoxingTraining.DAL.Repositories;
using BoxingTraining.BLL.Helpers;

namespace BoxingTraining.BLL.Services
{
    public class SportsmanService : GenericService<SportUser, SportUserDTO>, ISportsmanService
    {
        ISportsmanRepository _sportsmanRepository;
        ITrainerSportsmanRepository _trainerSportsmanRepository;
        IRoundStatisticService _roundStatisticService;
        public SportsmanService(ISportsmanRepository sportsmanRepository, ITrainerSportsmanRepository trainerSportsmanRepository, IRoundStatisticService roundStatisticService) : base(sportsmanRepository)
        {
            _sportsmanRepository = sportsmanRepository;
            _trainerSportsmanRepository = trainerSportsmanRepository;
            _roundStatisticService = roundStatisticService;
        }
        public async Task<bool> CreateSportsmanWithBindToTeacher(CreateSportsmanDTO createSportsmanDTO)
        {
            
            var addedResult = await base.AddAsync(createSportsmanDTO.SportsmanInformation);
            if (addedResult)
            {
                var trainerSportsmanEntity = new TrainerSportsman();
                trainerSportsmanEntity.SportsmanId = createSportsmanDTO.SportsmanInformation.Id;
                trainerSportsmanEntity.TrainerId = createSportsmanDTO.TrainerId;

                var result = await _trainerSportsmanRepository.AddAsync(trainerSportsmanEntity);
                addedResult = OperationHelper.IsSuccessResult(result);
            }
            return addedResult;
        }
        public async void DeleteAll(Guid Id)
        {
            await _trainerSportsmanRepository.DeleteWithConditionAsync($" where SportsmanId = '{Id}'");
            await base.DeleteAsync(Id);
            await _roundStatisticService.DeleteWithConditionAsync($" where SportUserId = '{Id}'");           
        }
    }
}
