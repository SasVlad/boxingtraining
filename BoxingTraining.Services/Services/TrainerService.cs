﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Common;
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Interfaces;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace BoxingTraining.BLL.Services
{
    public class TrainerService : GenericService<SportUser,SportUserDTO>,ITrainerService
    {
        ITrainerRepository _trainerRepository;
        public TrainerService(ITrainerRepository trainerRepository):base(trainerRepository)
        {
            _trainerRepository = trainerRepository;
        }
        public async Task<List<SportUserDTO>> GetTrainerSportsmans(Guid trainerId)
        {
            var trainerSportsmanList = await _trainerRepository.GetTrainerSportsmansAsync(trainerId);
            var trainerSportsmansListDTO = Mapper.Map<IEnumerable<SportUser>, List<SportUserDTO>>(trainerSportsmanList);
            return trainerSportsmansListDTO;
        }
    }
}
