﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Interfaces;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoxingTraining.BLL.DTOs.View;
using AutoMapper;

namespace BoxingTraining.BLL.Services
{
    public class RoundStatisticService : GenericService<RoundStatistic, RoundStatisticDTO>, IRoundStatisticService
    {
        IRoundStatisticRepository _roundStatisticRepository;
        ISportUserRepository _sportUserRepository;
        public RoundStatisticService(IRoundStatisticRepository roundStatisticRepository,
            ISportUserRepository sportUserRepository) : base(roundStatisticRepository)
        {
            _roundStatisticRepository = roundStatisticRepository;
            _sportUserRepository = sportUserRepository;
        }

        public async Task<bool> AddOrUpdateRecordAsync(RoundStatisticDTO entityDto)
        {
            var getExistRecord = await GetFilterRoundsStatisticAsync(new FilterRoundsStatisticDTO
            {
                SportUserId = entityDto.SportUserId,
                DateFrom = entityDto.DateRecord
            });
            var isExist = getExistRecord.Count > 0;
            var addedResult = false;
            if (isExist)
            {
                var existedRecord = getExistRecord.FirstOrDefault();
                existedRecord.CountRounds += entityDto.CountRounds;
                addedResult = await base.UpdateAsync(existedRecord);
            }
            if (!isExist)
            {
                addedResult = await base.AddAsync(entityDto);
            }
            return addedResult;
        }

        public async Task<List<RoundStatisticDTO>> GetFilterRoundsStatisticAsync(FilterRoundsStatisticDTO filterRoundsStatisticDTO)
        {
            var coditionForQuery = $" where SportUserId = '{filterRoundsStatisticDTO.SportUserId}'";

            if (!string.IsNullOrEmpty(filterRoundsStatisticDTO.DateFrom))
            {
                coditionForQuery += $" and DateRecord >= '{filterRoundsStatisticDTO.DateFrom}'";
            }

            if (!string.IsNullOrEmpty(filterRoundsStatisticDTO.DateTo))
            {
                coditionForQuery += $" and DateRecord <= '{filterRoundsStatisticDTO.DateTo}'";
            }

            var allStatisticForCurrentUser = await _roundStatisticRepository.GetAllAsync(coditionForQuery);
            var mappedUserStatistics = Mapper.Map<IEnumerable<RoundStatistic>, List<RoundStatisticDTO>>(allStatisticForCurrentUser);
            return mappedUserStatistics;
        }
    }
}
