﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Interfaces;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Services
{
    public class TrainerSportsmanService : GenericService<TrainerSportsman, TrainerSportsmanDTO>, ITrainerSportsmanService
    {
        public TrainerSportsmanService(ITrainerSportsmanRepository trainerSportsmanRepository) : base(trainerSportsmanRepository)
        {
            //Init();
        }
    }
}
