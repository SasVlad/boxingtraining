﻿using AutoMapper;
using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.DTOs.View;
using BoxingTraining.BLL.Helpers;
using BoxingTraining.BLL.Interfaces;
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        IApplicationUserManagerService _applicationUserManagerService;
        ITrainerService _trainerService;
        public AuthenticationService(IApplicationUserManagerService applicationUserManagerService,
            ITrainerService trainerService)
        {
            _applicationUserManagerService = applicationUserManagerService;
            _trainerService = trainerService;
        }

        public async Task<Guid> SignIn(SignInDTO signInDTO)
        {
            var user = await _applicationUserManagerService.FindAsync(signInDTO);
            if (user!=null)
            {
                return Guid.Parse(user.Id);
            }
            return Guid.Empty;           
        }
        public async Task<Guid> SignUp(TrainerViewDTO trainerViewDTO)
        {
            var resultSignUp = Guid.Empty;
            var addedResult = await _applicationUserManagerService.AddAsync(trainerViewDTO.AuthenticateInformation);
            var isSuccessAdded = addedResult;
            if (isSuccessAdded)
            {
                trainerViewDTO.GeneralInformation.Id = trainerViewDTO.AuthenticateInformation.Id;
                addedResult = await _trainerService.AddAsync(trainerViewDTO.GeneralInformation);
                resultSignUp = trainerViewDTO.GeneralInformation.Id;
            }
            return resultSignUp;
        }           
    }
}
