﻿using AutoMapper;
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Interfaces;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoxingTraining.BLL.Helpers;

namespace BoxingTraining.BLL.Services
{
    public abstract class GenericService<TEntity, TEntityDTO> : IGenericService<TEntity, TEntityDTO>
        where TEntity : BaseEntity
        where TEntityDTO : class
    {
        private IGenericRepository<TEntity> _genericRepository;
        public GenericService(IGenericRepository<TEntity> genericRepository)
        {
            _genericRepository = genericRepository;
        }
        public virtual async Task<bool> AddAsync(TEntityDTO entityDto)
        {
            var entity =
                Mapper.Map<TEntityDTO, TEntity>(entityDto);
            var addedResult = await _genericRepository.AddAsync(entity);
            return OperationHelper.IsSuccessResult(addedResult);
        }

        public virtual async Task<bool> DeleteAsync(Guid Id)
        {
            var deletedResult = await _genericRepository.DeleteAsync(Id);
            return OperationHelper.IsSuccessResult(deletedResult);
        }
        public virtual async Task<bool> DeleteWithConditionAsync(string where = null)
        {
            var deletedResult = await _genericRepository.DeleteWithConditionAsync(where);
            return OperationHelper.IsSuccessResult(deletedResult);
        }
        public virtual async Task<List<TEntityDTO>> GetAllAsync(string where = null)
        {
            var entitiesList =
                Mapper.Map<IEnumerable<TEntity>, List<TEntityDTO>>(await _genericRepository.GetAllAsync(where));
            return entitiesList;
        }

        public virtual async Task<bool> UpdateAsync(TEntityDTO entityDto)
        {
            var entity =
                Mapper.Map<TEntityDTO, TEntity>(entityDto);
            var updatedResult = await _genericRepository.UpdateAsync(entity);
            return OperationHelper.IsSuccessResult(updatedResult);
        }

        public virtual async Task<TEntityDTO> GetAsync(Guid Id)
        {
            var entity =
               Mapper.Map<TEntity, TEntityDTO>(await _genericRepository.GetAsync(Id));
            return entity;
        }
    }
}
