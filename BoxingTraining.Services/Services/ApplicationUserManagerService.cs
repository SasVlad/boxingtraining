﻿using AutoMapper;
using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.DTOs.View;
using BoxingTraining.BLL.Helpers;
using BoxingTraining.BLL.Interfaces;
using BoxingTraining.DAL.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Services
{
    public class ApplicationUserManagerService : IApplicationUserManagerService
    {
        UserManager<ApplicationUser> _applicationUserManager;
        public ApplicationUserManagerService(UserManager<ApplicationUser> applicationUserManager)
        {
            _applicationUserManager = applicationUserManager;
        }

        public async Task<bool> AddAsync(SignInDTO entityDto)
        {
            var applicationUser = Mapper.Map<SignInDTO, ApplicationUser>(entityDto);
            var identityResult = _applicationUserManager.Create(applicationUser, entityDto.Password);
            return identityResult.Succeeded;
        }

        public Task<bool> DeleteAsync(Guid Id)
        {
            throw new NotImplementedException();
        }

        public Task<List<SignInDTO>> GetAllAsync(string where = null)
        {
            throw new NotImplementedException();
        }

        public Task<SignInDTO> GetAsync(Guid Id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAsync(SignInDTO entity)
        {
            throw new NotImplementedException();
        }
        public async Task<ApplicationUserDTO> FindAsync(SignInDTO entity)
        {
            var user = await _applicationUserManager.FindAsync(entity.Email, entity.Password);
            var applicationUserDTO = Mapper.Map<ApplicationUser, ApplicationUserDTO>(user);
            return applicationUserDTO;
        }

        public Task<bool> DeleteWithConditionAsync(string where = null)
        {
            throw new NotImplementedException();
        }
    }
}
