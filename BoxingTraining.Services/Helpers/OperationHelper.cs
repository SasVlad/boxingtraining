﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.Helpers
{
    public static class OperationHelper
    {
        public static bool IsSuccessResult(int countSuccessOperationRecords)
        {
            return countSuccessOperationRecords > 0;
        }
    }
}
