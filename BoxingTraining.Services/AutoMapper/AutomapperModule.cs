﻿using AutoMapper;
using BoxingTraining.BLL.AutoMapper.Profilers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new RoundStatisticProfiler());
                cfg.AddProfile(new SportsmanProfiler());
                cfg.AddProfile(new TrainerProfiler());
                cfg.AddProfile(new TrainerSportsmanProfiler());
                cfg.AddProfile(new ApplicationUserProfiler());
            });
        }
    }
}
