﻿using AutoMapper;
using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.AutoMapper.Profilers
{
    public class TrainerProfiler : Profile
    {
        public TrainerProfiler()
        {
            CreateMap<SportUser, SportUserDTO>();
            CreateMap<SportUserDTO, SportUser>();
        }
    }
}
