﻿using AutoMapper;
using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.DAL.Entities;
using BoxingTraining.BLL.DTOs.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.BLL.AutoMapper.Profilers
{
    public class ApplicationUserProfiler : Profile
    {
        public ApplicationUserProfiler()
        {
            CreateMap<ApplicationUser, SignInDTO>();
            CreateMap<ApplicationUser, ApplicationUserDTO>();
            CreateMap<SignInDTO, ApplicationUser>()
                .ForMember("UserName", opt => opt.MapFrom(c => c.Email));
        }
    }
}
