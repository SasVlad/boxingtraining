namespace BoxingTraining.DAL.Migrations
{
    using BoxingTraining.DAL.Common;
    using BoxingTraining.DAL.Entities;
    using BoxingTraining.DAL.Identity;
    using BoxingTraining.DAL.Interfaces;
    using BoxingTraining.DAL.Repositories;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;

    internal sealed class Configuration : DbMigrationsConfiguration<BoxingTraining.DAL.BoxingTrainingContext>
    {
        IInitializeDatabase _initializeDatabase;

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            var _context = new BoxingTrainingContext();
            var _applicationUserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_context));
            _initializeDatabase = new InitializeDatabase(_applicationUserManager);
        }

        protected async override void Seed(BoxingTraining.DAL.BoxingTrainingContext context)
        {
            using (var ctx = new BoxingTrainingContext())
            {
                using (var writer = new XmlTextWriter($@"{AppDomain.CurrentDomain.BaseDirectory}\BoxingTrainingModel.edmx", Encoding.Default))
                {
                    EdmxWriter.WriteEdmx(ctx, writer);
                }
            }

            var trainer = _initializeDatabase.CreateTrainer().Result;
            var sportsman = _initializeDatabase.CreateSportsman().Result;
            var trainerSportsman = _initializeDatabase.CreateTrainerSportsman(trainer.Id, sportsman.Id);
            var roundStatisticTtrainer = _initializeDatabase.CreateRoundStatistic(trainer.Id,DateTime.Now, 50);
            var roundStatisticTtrainer1 = _initializeDatabase.CreateRoundStatistic(trainer.Id, DateTime.Now.AddDays(-1), 10);
            var roundStatisticTtrainer2 = _initializeDatabase.CreateRoundStatistic(trainer.Id, DateTime.Now.AddDays(-2), 60);
            var roundStatisticTtrainer3 = _initializeDatabase.CreateRoundStatistic(trainer.Id, DateTime.Now.AddDays(-3), 40);
            var roundStatisticSportsman = _initializeDatabase.CreateRoundStatistic(sportsman.Id, DateTime.Now, 50);
            var roundStatisticSportsman1 = _initializeDatabase.CreateRoundStatistic(sportsman.Id, DateTime.Now.AddDays(-1), 100);
            var roundStatisticSportsman2 = _initializeDatabase.CreateRoundStatistic(sportsman.Id, DateTime.Now.AddDays(-2), 20);
            var roundStatisticSportsman3 = _initializeDatabase.CreateRoundStatistic(sportsman.Id, DateTime.Now.AddDays(-3), 60);
        }

    }
}
