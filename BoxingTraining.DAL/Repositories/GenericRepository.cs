﻿using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Repositories
{
    public abstract class GenericRepository<TEntity>: IGenericRepository<TEntity> where TEntity:BaseEntity 
    {
        public string _connectionString = ConfigurationManager.ConnectionStrings["BoxingTrainingDb"].ConnectionString;

        public virtual async Task<int> AddAsync(TEntity entity)
        {    
            var columns = GetColumns();
            var stringOfColumns = string.Join(", ", columns);
            var stringOfParameters = string.Join(", ", columns.Select(e => "@" + e));
            var query = $"insert into {typeof(TEntity).Name}s ({stringOfColumns}) values ({stringOfParameters})";
            var result = 0;
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                try
                {
                    result = await connection.ExecuteAsync(query, entity);
                }
                catch (Exception ex)
                {

                    throw;
                }
                
            }
            return result;
        }

        public virtual async Task<int> DeleteAsync(Guid Id)
        {
            var query = $"delete from {typeof(TEntity).Name}s where Id = @Id";
            var result = 0;
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                result = await connection.ExecuteAsync(query,new { Id = Id });
            }
            return result;
        }
        public virtual async Task<int> DeleteWithConditionAsync(string where = null)
        {
            var query = $"delete from {typeof(TEntity).Name}s ";

            if (!string.IsNullOrWhiteSpace(where))
                query += where;

            var result = 0;
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                result = await connection.ExecuteAsync(query);
            }
            return result;
        }
        public virtual async Task<int> UpdateAsync(TEntity entity)
        {           
                var columns = GetColumnsWithoutId();
                var stringOfColumns = string.Join(", ", columns.Select(e => $"{e} = @{e}"));
                var query = $"update {typeof(TEntity).Name}s set {stringOfColumns} where Id = @Id";
                var result = 0;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    result = await connection.ExecuteAsync(query, entity);
                }
                return result;
        }

        public virtual async Task<TEntity> GetAsync(Guid Id)
        {
            var query = $"select * from {typeof(TEntity).Name}s where Id=@Id";

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return (await connection.QueryAsync<TEntity>(query, new { Id = Id })).FirstOrDefault();
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(string where = null)
        {
            var query = $"select * from {typeof(TEntity).Name}s ";

            if (!string.IsNullOrWhiteSpace(where))
                query += where;

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return await connection.QueryAsync<TEntity>(query);
            }
        }
        private IEnumerable<string> GetColumns()
        {
            return typeof(TEntity)
                    .GetProperties()
                    .Where(e => !e.GetMethod.IsVirtual)
                    .Select(e => e.Name);
        }
        private IEnumerable<string> GetColumnsWithoutId()
        {
            return typeof(TEntity)
                    .GetProperties()
                    .Where(e => e.Name != "Id" && !e.GetMethod.IsVirtual)
                    .Select(e => e.Name);
        }
    }
}
