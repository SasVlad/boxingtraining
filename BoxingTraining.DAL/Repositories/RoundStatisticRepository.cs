﻿using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Repositories
{
    public class RoundStatisticRepository : GenericRepository<RoundStatistic>, IRoundStatisticRepository
    {
    }
}
