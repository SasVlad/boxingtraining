﻿using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Entities.Enum;
using BoxingTraining.DAL.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Repositories
{
    public class SportsmanRepository : SportUserRepository, ISportsmanRepository
    {
        public override Task<IEnumerable<SportUser>> GetAllAsync(string where = null)
        {
            where = $" where SportUserType = {(int)SportUserType.Sportsman}";
            return base.GetAllAsync(where);
        }
        public override Task<int> AddAsync(SportUser entity)
        {
            entity.SportUserType = SportUserType.Sportsman;
            return base.AddAsync(entity);
        }
    }
}
