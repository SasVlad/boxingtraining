﻿using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Entities.Enum;
using BoxingTraining.DAL.Identity;
using BoxingTraining.DAL.Interfaces;
using Dapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Repositories
{
    public class TrainerRepository : SportUserRepository, ITrainerRepository
    {
        private ApplicationUserManager _applicationUserManager;
        private BoxingTrainingContext _context;
        public TrainerRepository():base()
        {
            _context = new BoxingTrainingContext();
            _applicationUserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_context));
        }
        public override Task<IEnumerable<SportUser>> GetAllAsync(string where = null)
        {
            where = $" where SportUserType = {(int)SportUserType.Trainer}";
            return base.GetAllAsync(where);
        }
        public override Task<int> AddAsync(SportUser entity)
        {
            entity.SportUserType = SportUserType.Trainer;
            return base.AddAsync(entity);
        }
        public async Task<IEnumerable<SportUser>> GetTrainerSportsmansAsync(Guid trainerId)
        {

            var query = $@"SELECT s.* FROM  {typeof(TrainerSportsman).Name}s ts INNER JOIN {typeof(SportUser).Name}s s ON ts.SportsmanId = s.Id  WHERE ts.TrainerId = @trainerId";

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                return await connection.QueryAsync<SportUser>(query, new { trainerId = trainerId });
            }
        }
    }
}
