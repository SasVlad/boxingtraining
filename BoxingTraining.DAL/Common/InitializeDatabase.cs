﻿using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Identity;
using BoxingTraining.DAL.Interfaces;
using BoxingTraining.DAL.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Common
{
    public class InitializeDatabase : IInitializeDatabase
    {
        UserManager<ApplicationUser> _applicationUserManager;
        public InitializeDatabase(UserManager<ApplicationUser> applicationUserManager)
        {
            _applicationUserManager = applicationUserManager;
        }
        public async Task<SportUser> CreateTrainer()
        {

            var trainer = new SportUser();
            trainer.FirstAndLastName = "Vasili234y Nicolaevich";

            var trainer1Email = "trainer1@gmail.com";
            var trainer1Password = "123456";

            var applicationUser = new ApplicationUser();
            applicationUser.Email = trainer1Email;
            applicationUser.UserName = trainer1Email;

            var identityResult = _applicationUserManager.Create(applicationUser, trainer1Password);
            if (identityResult.Succeeded)
            {
                trainer.Id = Guid.Parse(applicationUser.Id);
                var resultAdding = await new TrainerRepository().AddAsync(trainer);
            }

            return trainer;
        }
        public async Task<SportUser> CreateSportsman()
        {
            var sportsman = new SportUser();
            sportsman.FirstAndLastName = "Sportsman Nikita";
            var resultAdding = await new SportsmanRepository().AddAsync(sportsman);
            return sportsman;
        }
        public async Task<TrainerSportsman> CreateTrainerSportsman(Guid trainerId, Guid sportsmanId)
        {
            var trainerSportsman = new TrainerSportsman();
            trainerSportsman.SportsmanId = sportsmanId;
            trainerSportsman.TrainerId = trainerId;
            var resultAdding = await new TrainerSportsmanRepository().AddAsync(trainerSportsman);
            return trainerSportsman;
        }
        public async Task<RoundStatistic> CreateRoundStatistic(Guid personId,DateTime date, int countRound)
        {
            var roundStatistic = new RoundStatistic();
            roundStatistic.SportUserId = personId;
            roundStatistic.CountRounds = countRound;
            roundStatistic.DateRecord = date.ToShortDateString();
            var resultAdding = await new RoundStatisticRepository().AddAsync(roundStatistic);
            return roundStatistic;
        }
    }
}
