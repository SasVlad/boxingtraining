﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Interfaces
{
    public interface ITrainerRepository : IGenericRepository<SportUser>
    {
        Task<IEnumerable<SportUser>> GetTrainerSportsmansAsync(Guid trainerId);
    }
}
