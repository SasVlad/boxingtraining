﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Interfaces
{
    public interface IInitializeDatabase
    {
        Task<SportUser> CreateTrainer();
        Task<SportUser> CreateSportsman();
        Task<TrainerSportsman> CreateTrainerSportsman(Guid trainerId, Guid sportsmanId);
        Task<RoundStatistic> CreateRoundStatistic(Guid personId,DateTime date, int countRound);

    }
}
