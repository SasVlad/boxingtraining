﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.EntitiesConfiguration
{
    public class SportUserEntityConfiguration : EntityTypeConfiguration<SportUser>
    {
        public SportUserEntityConfiguration()
        {
            this.ToTable("SportUsers");
            this.HasKey<Guid>(s => s.Id);
            this.Property(p => p.FirstAndLastName)
                    .IsRequired();
        }
    }
}
