﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.EntitiesConfiguration
{
    public class TrainerSportsmanConfiguration : EntityTypeConfiguration<TrainerSportsman>
    {
        public TrainerSportsmanConfiguration()
        {
            this.ToTable("TrainerSportsmans");
            this.HasKey<Guid>(s => s.Id);
            this.HasRequired(e => e.Sportsman)
                 .WithMany()
                 .HasForeignKey(x => x.SportsmanId)
                 .WillCascadeOnDelete(false);

            this.HasRequired(e => e.Trainer)
                 .WithMany()
                 .HasForeignKey(x => x.TrainerId)
                 .WillCascadeOnDelete(false);
        }
    }
}
