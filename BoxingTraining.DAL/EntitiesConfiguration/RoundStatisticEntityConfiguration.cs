﻿using BoxingTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.EntitiesConfiguration
{
    public class RoundStatisticEntityConfiguration : EntityTypeConfiguration<RoundStatistic>
    {
        public RoundStatisticEntityConfiguration()
        {
            this.ToTable("RoundStatistics");
            this.HasKey<Guid>(s => s.Id);

            this.Property(x => x.CountRounds).IsRequired();
            this.Property(x => x.DateRecord).IsRequired();
        }
    }
}
