﻿using BoxingTraining.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL
{
    public class BoxingTrainingContext: IdentityDbContext<ApplicationUser>
    {
        public BoxingTrainingContext():base("BoxingTrainingDb"){}

        public DbSet<SportUser> SportUsers { get; set; }
        public DbSet<RoundStatistic> RoundStatistics { get; set; }
        public DbSet<TrainerSportsman> TrainerSportsmans { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
