﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Entities.Enum
{
    public enum SportUserType
    {
        Trainer = 0,
        Sportsman = 1
    }
}
