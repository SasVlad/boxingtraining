﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Entities
{
    public class RoundStatistic:BaseEntity
    {
        public Guid SportUserId { get; set; }
        public int CountRounds { get; set; }
        public string DateRecord { get; set; }
    }
}
