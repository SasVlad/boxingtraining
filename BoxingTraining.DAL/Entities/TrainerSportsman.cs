﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Entities
{
    public class TrainerSportsman : BaseEntity
    {
        public Guid TrainerId { get; set; }
        public Guid SportsmanId { get; set; }       
        public virtual SportUser Sportsman { get; set; }
        public virtual SportUser Trainer { get; set; }
    }
}
