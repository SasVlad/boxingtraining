﻿using BoxingTraining.DAL.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DAL.Entities
{
    public class SportUser:BaseEntity
    {
        public string FirstAndLastName { get; set; }
        public SportUserType SportUserType { get; set; }
    }
}
