﻿using BoxingTraining.DAL;
using BoxingTraining.DAL.Common;
using BoxingTraining.DAL.Entities;
using BoxingTraining.DAL.Identity;
using BoxingTraining.DAL.Interfaces;
using BoxingTraining.DAL.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DependencyInjection.NinjectModules
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IGenericRepository<>)).To(typeof(GenericRepository<>));
            Bind(typeof(IdentityDbContext<>)).To(typeof(BoxingTrainingContext));
            Bind(typeof(IUserStore<>)).To(typeof(UserStore<>)).WithConstructorArgument("context", ctx => ctx.Kernel.Get<IdentityDbContext<ApplicationUser>>());
            Bind(typeof(UserManager<>)).To(typeof(ApplicationUserManager)).WithConstructorArgument("store", ctx => ctx.Kernel.Get<IUserStore<ApplicationUser>>()); 
            Bind<IRoundStatisticRepository>().To<RoundStatisticRepository>();
            Bind<ISportsmanRepository>().To<SportsmanRepository>();
            Bind<ITrainerRepository>().To<TrainerRepository>();
            Bind<ITrainerSportsmanRepository>().To<TrainerSportsmanRepository>();
            Bind<IInitializeDatabase>().To<InitializeDatabase>();
            Bind<ISportUserRepository>().To<SportUserRepository>();
        }
    }
}
