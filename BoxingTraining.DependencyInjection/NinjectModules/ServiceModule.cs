﻿using BoxingTraining.BLL.Services;
using BoxingTraining.BLL.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingTraining.DependencyInjection.NinjectModules
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IGenericService<,>)).To(typeof(GenericService<,>));
            Bind<IRoundStatisticService>().To<RoundStatisticService>();
            Bind<ISportsmanService>().To<SportsmanService>();
            Bind<ITrainerService>().To<TrainerService>();
            Bind<ITrainerSportsmanService>().To<TrainerSportsmanService>();
            Bind<IAuthenticationService>().To<AuthenticationService>();
            Bind<IApplicationUserManagerService>().To<ApplicationUserManagerService>();
        }
    }
}
