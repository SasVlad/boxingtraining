import {SportUser } from "../entity/sport-user";
import { SignIn } from "./sign-in";

export class TrainerView {
    constructor() {
      this.GeneralInformation = new SportUser();
      this.AuthenticateInformation = new SignIn();
    }
  GeneralInformation: SportUser;
  AuthenticateInformation: SignIn;
}
