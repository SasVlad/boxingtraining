"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sport_user_1 = require("../entity/sport-user");
var sign_in_1 = require("./sign-in");
var TrainerView = /** @class */ (function () {
    function TrainerView() {
        this.GeneralInformation = new sport_user_1.SportUser();
        this.AuthenticateInformation = new sign_in_1.SignIn();
    }
    return TrainerView;
}());
exports.TrainerView = TrainerView;
//# sourceMappingURL=trainer-view.js.map