﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.Services;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BoxingTraining.API.Controllers
{
    public class TrainerController : ApiController
    {
        private ITrainerService _trainerService;
        public TrainerController(ITrainerService trainerService)
        {
            _trainerService = trainerService;
        }
        public async Task<IHttpActionResult> Get()
        {
            var trainersList = await _trainerService.GetAllAsync();
            return Ok(trainersList);
        }
        public async Task<IHttpActionResult> Get(Guid Id)
        {
            var trainer = await _trainerService.GetAsync(Id);
            return Ok(trainer);
        }
        public async Task<IHttpActionResult> Post(SportUserDTO SportUserDTO)
        {
            var addedResalt = await _trainerService.AddAsync(SportUserDTO);
            return Ok(addedResalt);
        }
        public async Task<IHttpActionResult> Put(SportUserDTO trainerDTO)
        {
            var updateResalt = await _trainerService.UpdateAsync(trainerDTO);
            return Ok(updateResalt);
        }
        public async Task<IHttpActionResult> Delete(Guid Id)
        {
            var deleteResalt = await _trainerService.DeleteAsync(Id);
            return Ok(deleteResalt);
        }
        public async Task<IHttpActionResult> GetTrainerSportsmans(Guid Id)
        {
            var trainerSportsmansList = await _trainerService.GetTrainerSportsmans(Id);
            return Ok(trainerSportsmansList);
        }
    }
}
