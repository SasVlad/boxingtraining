﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.Services;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BoxingTraining.BLL.DTOs.View;

namespace BoxingTraining.API.Controllers
{
    public class SportsmanController : ApiController
    {
        private ISportsmanService _sportsmanService;
        public SportsmanController(ISportsmanService sportsmanService)
        {
            _sportsmanService = sportsmanService;
        }
        public async Task<IHttpActionResult> Get()
        {
            var sportsmansList = await _sportsmanService.GetAllAsync();
            return Ok(sportsmansList);
        }
        public async Task<IHttpActionResult> Post(CreateSportsmanDTO createSportsmanDTO)
        {
            var addedResalt = await _sportsmanService.CreateSportsmanWithBindToTeacher(createSportsmanDTO);
            if (addedResalt)
            {
                return Ok();
            }
            return BadRequest();
        }
        public async Task<IHttpActionResult> Put(SportUserDTO sportsmanDTO)
        {
            var updateResalt = await _sportsmanService.UpdateAsync(sportsmanDTO);
            return Ok(updateResalt);
        }
        public IHttpActionResult Delete(Guid Id)
        {
            _sportsmanService.DeleteAll(Id);
            return Ok();
        }
    }
}
