﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.DTOs.View;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BoxingTraining.API.Controllers
{
    public class AuthenticationController : ApiController
    {
        private IAuthenticationService _authenticationService;
        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public async Task<IHttpActionResult> SignIn(SignInDTO signInDTO)
        {
            var user = await _authenticationService.SignIn(signInDTO);
            if (user!=Guid.Empty)
            {
                return Ok(user);               
            }
            return Unauthorized();
        }

        public async Task<IHttpActionResult> SignUp(TrainerViewDTO trainerViewDTO)
        {
            var trainerGuid = await _authenticationService.SignUp(trainerViewDTO);
            if (trainerGuid != Guid.Empty)
            {
                return Ok(trainerGuid);
            }
            return BadRequest();
        }
    }
}
