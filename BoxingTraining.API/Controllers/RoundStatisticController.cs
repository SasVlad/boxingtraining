﻿using BoxingTraining.BLL.DTOs.Entity;
using BoxingTraining.BLL.Services;
using BoxingTraining.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BoxingTraining.BLL.DTOs.View;

namespace BoxingTraining.API.Controllers
{
    public class RoundStatisticController : ApiController
    {
        private IRoundStatisticService _roundStatisticService;
        public RoundStatisticController(IRoundStatisticService roundStatisticService)
        {
            _roundStatisticService = roundStatisticService;
        }
        public async Task<IHttpActionResult> PostFilterRoundsStatisticAsync(FilterRoundsStatisticDTO filterRoundsStatisticDTO)
        {
            var roundStatisticsList = await _roundStatisticService.GetFilterRoundsStatisticAsync(filterRoundsStatisticDTO);
            return Ok(roundStatisticsList);
        }
        public async Task<IHttpActionResult> Get()
        {
            var roundStatisticsList = await _roundStatisticService.GetAllAsync();
            return Ok(roundStatisticsList);
        }

        public async Task<IHttpActionResult> Post(RoundStatisticDTO roundStatisticDTO)
        {
            var addedResalt = await _roundStatisticService.AddOrUpdateRecordAsync(roundStatisticDTO);
            return Ok(addedResalt);
        }
        public async Task<IHttpActionResult> Put(RoundStatisticDTO roundStatisticDTO)
        {
            var updateResalt = await _roundStatisticService.UpdateAsync(roundStatisticDTO);
            return Ok(updateResalt);
        }
        public async Task<IHttpActionResult> Delete(Guid Id)
        {
            var deleteResalt = await _roundStatisticService.DeleteAsync(Id);
            return Ok(deleteResalt);
        }
    }
}
